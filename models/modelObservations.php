<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 28.03.2019
 * Time: 11:11
 * File: modelObservations.php
 */

/*
 * permet d'aller chercher les observations dans la base de donnée
 * */
function LoadObservations()
{
    $dataDirectory = "database";
    $dataFileName = "observations.json";

    if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
        if ($data == false) {
            $data = array();
        }
    } else {
        //TODO - page erreur systeme
    }

    return $data;
}

/*
 * permet de récupérer une observation précise de la base de donnée
 * */
function LoadObservation($idObservation)
{
    $dataDirectory = "database";
    $dataFileName = "observations.json";

    $data = file_exists("$dataDirectory/$dataFileName") ? json_decode(file_get_contents("$dataDirectory/$dataFileName"), true) : "error"; //

    foreach ($data as $obs) {
        if ($obs['ID'] == $idObservation) {
            $observation = $obs;
            break;
        }
    }

    return $observation;
}

/*
 * permet de mettre a jour une observation dans la base de donnée
 * */
function updateObservation($newdata)
{

    $idObservation = $newdata['id'];
    $dataDirectory = "../database";
    $dataFileName = "observations.json";

    $data = file_exists("$dataDirectory/$dataFileName") ? json_decode(file_get_contents("$dataDirectory/$dataFileName"), true) : "error"; // the file already exists -> load it
    foreach ($data as $key => $value) {

        if ($value['ID'] == $idObservation) {
            //update observation
            $data[$key]["Nom"] = $newdata['title'];
            $data[$key]["Desc"] = $newdata['desc'];
            $data[$key]["Date"] = $newdata['date'];
            $data[$key]["PosX"] = $newdata['x'];
            $data[$key]["PosY"] = $newdata['y'];
        }
    }

    $newJsonString = json_encode($data);
    return file_put_contents("$dataDirectory/$dataFileName", $newJsonString) ? true : false;
}

/*
 * permet de supprimer une image de la base de donnée et des fichiers
 * */
function deleteImageFromObservation($image, $idObservation)
{
    $validated = false;
    $dataDirectory = "database";
    $dataFileName = "observations.json";
    $data = file_exists("$dataDirectory/$dataFileName") ? json_decode(file_get_contents("$dataDirectory/$dataFileName"), true) : "error"; //

    foreach ($data as $keyobs => $obs) {
        if ($obs['ID'] == $idObservation) {
            foreach ($obs['Photos'] as $keypics => $pics) {
                if ($image == $pics) {
                    unset($data[$keyobs]['Photos'][$keypics]);
                    $newJsonString = json_encode($data);
                    file_put_contents("$dataDirectory/$dataFileName", $newJsonString) ? $validated = true : $validated = false;
                    break;
                }
            }

        }
    }


    return $validated;
}

/*
 * permettre d'upload une image et de la mettre dans la base de donnée
 * */
function uploadImage($file, $idObservation)
{
    session_start();
    $dataDirectory = "../database";
    $dataFileName = "observations.json";
    $name = $file['name'];
    $target_dir = "../views/resources/images/observations/" . $_SESSION['userPseudo'] . "/";
    $target_file = $target_dir . basename($file["name"]);

    if (!is_dir($target_dir)) {
        mkdir($target_dir);
    }

    // Select file type
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    // Valid file extensions
    $extensions_arr = array("jpg", "jpeg", "png", "gif");

    // Check extension
    if (in_array($imageFileType, $extensions_arr)) {


        // Upload file
        move_uploaded_file($file['tmp_name'], $target_file) ? $coucou = true : $coucou = false;

        //add picture to database
        $Observations = file_exists("$dataDirectory/$dataFileName") ? json_decode(file_get_contents("$dataDirectory/$dataFileName"), true) : "error"; // the file already exists -> load it

        foreach ($Observations as $key => $Observation) {
            if ($idObservation == $Observation['ID']) {
                $obCount = count($Observation['Photos']);
                $Observations[$key]['Photos'][$obCount] = basename($file["name"]);
            }
        }

        $newJsonString = json_encode($Observations);
        file_put_contents("$dataDirectory/$dataFileName", $newJsonString);
    }

    return $coucou;
}

/*
 * permettre de sauvegarder les modifications apportées à une observation
 * */
function saveObservationModifications($data)
{
    $dataDirectory = "database";
    $dataFileName = "observations.json";

    $newJsonString = json_encode($data);
    file_put_contents("$dataDirectory/$dataFileName", $newJsonString);
}

/*
 * permet d'enregistrer dans la base de donnée une obseravtion
 * */
function obsRegisterRequest($newData)
{
    $dataDirectory = "database";
    $dataFileName = "observations.json";


    $data = LoadObservations();

    $data[] = $newData;

    $data = json_encode($data);
    file_put_contents("$dataDirectory/$dataFileName", $data);
}
