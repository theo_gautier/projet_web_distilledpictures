<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 21.03.2019
 * Time: 12:08
 * File: model.php
 */
/*
 * permet de récupérer les informations sur les utilisateurs dupis la base de donnée
 * */
function LoadfileUser(){
    $dataDirectory = "database";
    $dataFileName = "userData.json";

    if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("$dataDirectory/$dataFileName"),true);
        if($data == false){
            $data = array();
        }
    }else{
        //TODO - page erreur systeme
    }

    return $data;
}

/*
 * permet d'écrire les nouvelles données dans la base de donnée
 * */
function writeFile($data){
    $dataDirectory = "database";
    $dataFileName = "userData.json";
    file_put_contents("$dataDirectory/$dataFileName", json_encode($data));
}

/*
 * permet de récupérer toutes les informations sur un utilisateur depuis la base de donnée
 * */
function UserLoginRequest($email){

    $resultdata = LoadfileUser();


    $result['error'] = "systemresulterror";

        foreach ($resultdata as $resultline) {
            if ($resultline['email'] == $email) {
                $result['error'] = "done";
                $result['email'] = $resultline['email'];
                $result['pseudo'] = $resultline['pseudo'];
                $result['firstname'] = $resultline['firstname'];
                $result['name'] = $resultline['name'];
                $result['password'] = $resultline['password'];
            }
        }

    return $result;
}

/*
 * permet d'enregistrer un nouvel utilisateur dans la base de donnée
 * */
function registerRequest($newdata){

    $dataDirectory = "database";
    $dataFileName = "userData.json";



    $data = LoadfileUser();

    $data[] = $newdata;

    $data = json_encode($data);





    file_put_contents("$dataDirectory/$dataFileName", $data );
}

/*
 * permet de récupérer un email d'utilisateur en fonction de son pseudo
 * */
function UserEmailRequest($pseudo){

    $resultdata = LoadfileUser();


    $result['error'] = "systemresulterror";

    foreach ($resultdata as $resultline) {
        if ($resultline['pseudo'] == $pseudo) {
            $result['email'] = $resultline['email'];
            $result['error'] = "done";
        }
    }

    return $result;
}