<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 21.03.2019
 * Time: 11:30
 * File: controllerUser.php
 */

/*
 * permet d'appeler la view de la page d'acceuil
 * */
function showAccueil()
{
    require_once 'views/accueil.php';
}

/*
 * permet de sois appeler la view du login sois préperer le login
 * */
function login()
{
    if (!isset($_POST['InputEmail'])) {
        require_once 'views/login.php';
    } else {
        require_once 'models/modelUser.php';
        $email = $_POST['InputEmail'];
        $password = $_POST['InputPassword'];

        loginSession($email, $password);

    }


}

/*
 * permet de gérer la création des sessions d'utilisateur
 * */
function loginSession($email, $password)
{
    $result = UserLoginRequest($email);
    if ($result['error'] == 'done') {
        $hashpassword = $result['password'];
        if (password_verify($password, $hashpassword)) {

            $_SESSION['userEmail'] = $result['email'];
            $_SESSION['userPseudo'] = $result['pseudo'];
            $_SESSION['userName'] = $result['name'];
            $_SESSION['userFirstName'] = $result['firstname'];

            showAccueil();
        } else {
            $_POST['loginerror'] = "true";
            require_once 'views/login.php';
        }
    } else {
        $_POST['loginerror'] = "true";
        require_once 'views/login.php';
    }
}

/*
 * permet de sois afficher la page d'inscription sois enregistrer l'utilisateur avec les informations qu'il a fourni
 * */
function Register()
{
    if (!isset($_POST['InputEmail'])) {
        require_once 'views/forumlaireinscription.php';
    } else {
        $userEmail = $_POST['InputEmail'];
        $userPseudo = $_POST['InputPseudo'];
        $userpswd = $_POST['InputPassword'];
        $userpswdRespeat = $_POST['InputVPassword'];
        $userName = $_POST['InputNom'];
        $userFirstName = $_POST['InputPrenom'];

        if ($userpswd != $userpswdRespeat) {
            $_POST['registererror'] = "1";
            require_once "views/forumlaireinscription.php";
        } else {
            require_once 'models/modelUser.php';
            $result = UserLoginRequest($userEmail);
            if ($result['error'] == "systemresulterror") {
                $encryptedPswd = password_hash($userpswd, PASSWORD_DEFAULT);
                $newUser = new stdClass();

                $newUser->email = $userEmail;
                $newUser->pseudo = $userPseudo;
                $newUser->firstname = $userFirstName;
                $newUser->name = $userName;
                $newUser->password = $encryptedPswd;

                registerRequest($newUser);

                loginSession($userEmail, $userpswd);
            } else {
                $_POST['registererror'] = "1";
                require_once "views/forumlaireinscription.php";
            }
        }
    }
}

/*
 * permet de détruire les sessions
 * */
function logout()
{
    $_SESSION = array();
    session_destroy();

    showAccueil();
}

/*
 * permet d'afficher les dernieres vues
 * */
function displayLastViews()
{

    require_once "models/modelObservations.php";
    $observations = LoadObservations();

    $timestamptable = array();
    $contenu = "";

    foreach ($observations as $observation) {
        $timestamptable[] = strtotime(str_replace('.', '-', $observation['Date']));
    }
    $timestamptablefiltered = array();

    arsort($timestamptable);

    foreach ($timestamptable as $timest) {
        $tablesize = sizeof($timestamptablefiltered);
        if ($tablesize !== 0) {
            $tablesize = $tablesize - 1;

            if ($timest !== $timestamptablefiltered[$tablesize]) {
                $timestamptablefiltered[] = $timest;
            }

        } else {
            $timestamptablefiltered[] = $timest;
        }

    }


    foreach ($timestamptablefiltered as $item) {
        foreach ($observations as $observation) {
            if (strtotime(str_replace('.', '-', $observation['Date'])) === $item) {
                $contenu .= formattingObservationCard($observation);
            }
        }
    }

    /*
        foreach ($timestamptablefiltered as $value){
            echo $value."\n";
        }*/
    require_once "views/derniereobservation.php";
}

/*
 * permet de rechercher des vues
 * */
function displaySearchViews($posttable)
{
    require_once "models/modelObservations.php";

    if (isset($posttable['InputSearch'])) {
        $observations = LoadObservations();
        $contenu = "";
        $recherche = $posttable['InputSearch'];

        $recherche = strtolower($recherche);
        foreach ($observations as $observation) {

            if (strpos(strtolower($observation['Nom']), $recherche) !== false || strpos(strtolower($observation['Desc']), $recherche) !== false || strpos(strtolower($observation['Auteur']), $recherche) !== false || strpos(strtolower($observation['Date']), $recherche) !== false) {
                $contenu .= formattingObservationCard($observation);
            }

        }
        $active = 1;
    }

    if (isset($active) && $contenu == "") {
        $contenu = "Aucune observation n'a été trouvée";
    }

    require_once "views/rechercherobservations.php";
}

/*
 * permet d'afficher les vues de l'utilisateur
 * */
function displayMyViews()
{
    require_once "models/modelObservations.php";
    if (isset($_SESSION['userEmail'])) {
        $observations = LoadObservations();
        $contenu = "";
        foreach ($observations as $observation) {
            if ($observation['Auteur'] == $_SESSION['userPseudo']) {
                $contenu .= formattingObservationCard($observation);
            }
        }

        if ($contenu == "") {
            $contenu = "Vous n'avez posté encore aucune observation";
        }

        require_once "views/mesobservations.php";
    } else {
        require_once "views/pagederreur.php";
    }
}

/*
 *  permet d'afficher chaque vue
 * */
function displaySingleView($varGet)
{
    require_once "models/modelObservations.php";

    $observations = LoadObservations();
    $contenu = "";

    foreach ($observations as $observation) {
        if ($varGet['id'] == $observation['ID']) {


            $head = "<link rel=\"stylesheet\" type=\"text/css\" href=\"views/resources/TomTom/sdk/map.css\"/>
        <script src=\"views/resources/TomTom/sdk/tomtom.min.js\"></script>";
            $contenu = "<br>
<h1 class=\"dp-TitleOfPage\">" . $observation['Nom'] . "</h1>
<br>
<br><table border=\"5px\" width=\"75%\" align=\"center\">
    <tr>
        <td><p>Auteur: " . $observation['Auteur'] . "</p></td>
        <td rowspan=\"2\"><p>Descrption: " . $observation['Desc'] . "</p></td>
        <td rowspan=\"2\">
            <p>Emplacement</p> 
            <br>
            <div>".$observation['PosX'].", ".$observation['PosY']."</div>
       
        </td>";
            if (isset($_SESSION['userPseudo'])) {
                if ($observation['Auteur'] == $_SESSION['userPseudo']) {
                    $contenu .= "<td><a class=\"nav-link text-white\" href=\"index.php?action=modifier&id=" . $observation['ID'] . "\">Modifier</a></td>";
                }
            }
            $contenu .= "
    </tr>
    <tr>
        <td><p>Date: " . $observation['Date'] . "</p></td>";
            if (isset($_SESSION['userPseudo'])) {
                if ($observation['Auteur'] == $_SESSION['userPseudo']) {
                    $contenu .= "<td><a class=\"nav-link text-white\" href=\"index.php?action=supprimerObs&id=" . $observation['ID'] . "\"><p>Supprimer</p></a></td>";
                }
            }
            $contenu .= "</tr></table>";

            $contenu .= "<div class=\"d-flex flex-wrap justify-content-around\">";

            foreach ($observation['Photos'] as $image) {
                $contenu .= "<div class=\"p-2 border border-dark bg-secondary w-auto h-auto\"><img src=\"views/resources/images/observations/" . $observation['Auteur'] . "/" . $image . "\" class=\"img-fluid\" style='max-height: 200px;'></div>";
            }

            $contenu .= "</div>";

            if (isset($_SESSION['userPseudo'])) {
                if ($observation['Auteur'] !== $_SESSION['userPseudo']) {
                    $contenu .= "<br><br><div class=\"dp-ThatFormulaire mx-auto\"><form action=\"index.php?action=sendEmail\" method=\"post\" name=\"formEmail\">
                    <label for='mailSubject'>Sujet</label>
                    <input type='text' name='mailSubject' class='dp-InputText' id='mailSubject' required>
                    <label for='mailMessage'>Message</label>
                     <input type='text' name='mailMessage' class='dp-InputText' id='mailMessage' required>
                     <input type='text' value='".$observation['Nom']."' name='nomObservation' id='nomObservation' hidden>
                     <input type='text' value='".$observation['Auteur']."' name='mailTo' id='mailTo' hidden>
                     <br>
                     <input type=\"submit\" value=\"Envoyer\" class=\"btn btn-primary\">
                     <p>L'auteur de l'observation verra votre Email et votre pseudo dans le mail.</p></form></div>";
                }
            }
        }
    }

    if ($contenu == "") {
        $contenu = "Aucune observation ne porte cet ID";
    }
    require_once "views/singleObservation.php";
}

/*
permet de modifier une vue
*/
function displayModifyView($varGet)
{
    require_once "models/modelObservations.php";

    $observation = LoadObservation($varGet);

    require_once "views/modificatorObservation.php";
}

/*
 * permet de formatter les cartes des observations pour les afficher ensuite
 * */
function formattingObservationCard($observation)
{

    $nbimage = sizeof($observation['Photos']);
    if ($nbimage == 0) {
        $image0 = "../../site/noimage.jpg";
    } else {
        $image0 = $observation['Photos'][0];
    }
    $formatedObservation = "<div class=\"p-2 align-self-center card bg-dark\" style=''><a href='index.php?action=SingleView&id=" . $observation['ID'] . "' >
        <table class=\"border border-dark bg-secondary\">
            <tr>
                <td><img src=\"views/resources/images/observations/" . $observation['Auteur'] . "/" . $image0 . "\"  class='img-fluid card-img' style='max-height: 200px; width: auto; align-content: center; display: block; margin-left: auto; margin-right: auto'></td>
            </tr>
            <tr>
                <td>
                    <P>Titre: " . $observation['Nom'] . "</P>
                    <br>
                    <p>Nombre d'images: " . $nbimage . "</p>
                    <br>
                    <p>Date: " . $observation['Date'] . "</p>
                    
                </td>
            </tr>
        </table></a>
    </div>";

    return $formatedObservation;
}

/*
 * permet de supprimer une observation et ses images
 * */
function deleteObservation($varGet)
{
    require_once "models/modelObservations.php";

    $observations = LoadObservations();

    foreach ($observations as $key => $observation) {
        if ($varGet['id'] == $observation['ID']) {
            if (isset($_SESSION['userPseudo'])) {
                if ($observation['Auteur'] == $_SESSION['userPseudo']) {
                    foreach ($observation['Photos'] as $image) {
                        unlink("views/resources/images/observations/" . $_SESSION['userPseudo'] . "/" . $image);
                    }
                    unset($observations[$key]);
                    saveObservationModifications($observations);
                }
            }
        }
    }

    displayMyViews();

}

/*
 * permet de créer une vue
 * */
function createView()
{
    if (isset($_SESSION['userEmail'])) {
        if (!isset($_POST['InputTitreview'])) {
            require_once 'views/FormulaireCreationObservation.php';
        } else {
            require_once "models/modelObservations.php";

            $observations = LoadObservations();

            $higherID = 0;

            foreach ($observations as $observation) {
                if ($observation['ID'] > $higherID) {
                    $higherID = $observation['ID'];
                }
            }


            $obsTitle = $_POST['InputTitreview'];
            $obsDesc = $_POST['InputDescriptionView'];
            $obsDate = $_POST['InputDateView'];
            $obsPosX = $_POST['InputPlacementXView'];
            $obsPosY = $_POST['InputPlacementYView'];

            $obsDate = substr($obsDate,8,2).".".substr($obsDate,5,2).".".substr($obsDate,0,4);

            $newObs = new stdClass();


            $newObs->ID = $higherID + 1;
            $newObs->Nom = $obsTitle;
            $newObs->Date = $obsDate;
            $newObs->Desc = $obsDesc;
            $newObs->PosX = $obsPosX;
            $newObs->PosY = $obsPosY;
            $newObs->Auteur = $_SESSION['userPseudo'];
            $newObs->Vus = 0;
            $newObs->Photos = array();

            obsRegisterRequest($newObs);

            $vue['id'] = $newObs->ID;
            displaySingleView($vue);
        }
    } else {
        require_once "views/pagederreur.php";
    }
}

/*
 * permet d'envoyer un mail
 * */
function sendMail(){
    $mailTo = $_POST['mailTo'];
    $mailSubject = $_POST['mailSubject'];
    $mailMessage = $_POST['mailMessage'];
    $nomObservation = $_POST['nomObservation'];
    $userPseudo = $_SESSION['userPseudo'];
    $mailFrom = $_SESSION['userEmail'];
    $headers = 'From:'. "noreply@distilledpictures.mycpnv.ch" . "\r\n" . 'Reply-To:'. $mailFrom . "\r\n";

    require_once 'models/modelUser.php';
    $result = UserEmailRequest($mailTo);
    $mailTo = $result['email'];


    $mailMessage = "Vous avez recu un mail de ". $userPseudo ."pour votre observation nommée ". $nomObservation. ".\n\n-----------\n\n".$mailMessage;

    mail($mailTo,$mailSubject,$mailMessage,$headers);

	showAccueil();
}

