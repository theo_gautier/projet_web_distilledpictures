<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 21.03.2019
 * Time: 11:49
 * File: login.php
 */

ob_start();
$titre="DistilledPictures - Login";
?>
    <br>
    <h1 class="dp-TitleOfPage">Login</h1>
        <?php
        if(isset($_POST["loginerror"])){
            echo "<div class=\"alert alert-danger\" role=\"alert\"><h3 class=\"text-center font-weight-bolder align-middle\">email ou mot de passe éronné</h3></div>";
        }
        ?>
        <div class="dp-ThatFormulaire mx-auto">

            <form action="index.php?action=login" method="post" name="formLogin">

                <label for="inputEmail">Adresse Email</label>
                <input type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="exemple@exemple.com" required class="dp-InputText" name="InputEmail">


                <label for="inputPassword" >Mot De Passe</label>
                <input type="password" id="inputPassword" placeholder="Mot De Passe" required class="dp-InputText" name="InputPassword">


                <input type="submit" value="Submit" class="btn btn-primary">
            </form>
        </div>



<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>