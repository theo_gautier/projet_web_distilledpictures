<!DOCTYPE html>
<html lang="fr-CH">
<head>
    <meta charset="UTF-8">
    <title><?=$titre;?></title>
    <link rel="stylesheet" href="views/resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="views/resources/css/css.css">

    <script src="views/node_modules/jquery/dist/jquery.js"></script>
    <script src="views/resources/bootstrap/js/bootstrap.js"></script>
    <script src="views/resources/bootstrap/js/bootstrap.bundle.js"></script>

    <?php if(isset($head)){
        echo $head;
    } ?>

</head>
<body>
<div id="divMenu" >
    <div id="divImgMenu" >

        <div id="carouselImage" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="views/resources/images/site/Banniere1.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="views/resources/images/site/Banniere2.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="views/resources/images/site/Banniere3.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="views/resources/images/site/Banniere4.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="views/resources/images/site/Banniere5.jpg" class="d-block w-100 img-fluid">
                </div>
                <div class="carousel-item">
                    <img src="views/resources/images/site/Banniere6.jpg" class="d-block w-100 img-fluid">
                </div>
            </div>
        </div>

    </div>

    <!-- texte -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="views/resources/images/site/logo.png" style="max-height: 62px; width: auto; background-color: #90979d;">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#butonsnav" aria-controls="butonsnav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse border-colapse navbar-collapse" id="butonsnav">

            <ul id="divBtnMenu" class="navbar-nav mr-auto">
                <li class="nav-item active dp-border-1px-right dp-hide-border">
                    <a class="nav-link text-white" href="index.php?action=accueil">Accueil</a>
                </li>
                <li class="nav-item dp-border-1px-right dp-hide-border">
                    <a class="nav-link text-white" href="index.php?action=LastViews">Dernières observations</a>
                </li>
                <li class="nav-item dp-border-1px-right dp-hide-border">
                    <a class="nav-link text-white" href="index.php?action=SearchViews">Chercher observations</a>
                </li>
                <li class="nav-item dp-border-1px-right dp-hide-border">
                    <a class="nav-link text-white" href="index.php?action=MyViews">Mes observations</a>
                </li>

            </ul>
            <div class="nav-item navbar-nav  navbar-light bg-dark">
                <ul id="divBtnMenu" class="navbar-nav mr-auto">
                    <?php if(!isset($_SESSION['userEmail'])){
                        echo '<li class="nav-item dp-border-1px-right dp-border-1px-left dp-hide-border">
                        <a class="nav-link text-white" href="index.php?action=register">S\'inscrire</a>
                    </li>
                    <li class="nav-item dp-border-1px-right dp-hide-border">
                        <a class="nav-link text-white"  href="index.php?action=login">Login</a>
                    </li>';
                    }else{

                        echo '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$_SESSION['userPseudo'].'</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item text-white" href="index.php?action=logout">Se déconnecter</a>
                                 </div>
                              </li>';}

                    ?>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="span12" id="divMain">
    <?=$contenu; ?>
</div>

</body>
</html>