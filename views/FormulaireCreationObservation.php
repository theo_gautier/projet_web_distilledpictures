<?php
/**
 * Created by PhpStorm.
 * User: Michael.HOFER
 * Date: 11.03.2019
 * Time: 08:41
 */
//Ouverture de la lecture à implémenté dans le gabarit
ob_start();
$titre="DistilledPictures - Formulaire d'inscrption";
?>
<br>
<h1 class="dp-TitleOfPage">Formulaire d'inscription</h1>

<!--Mise en place du formulaire de création d'observation-->
    <div class="dp-ThatFormulaire mx-auto">

        <form action="index.php?action=createView" method="post" name="formCreationObservation">

            <label for="InputTitreView">Titre</label>
            <input type="text"  id="InputTitreview" aria-describedby="TitreviewHelp" placeholder="Titre de l'observation" required class="dp-InputText" name="InputTitreview">

            <label for="InputDescriptionView">Description *</label>
            <input type="text" id="InputDescriptionView" aria-describedby="DescriptionViewHelp" placeholder="Description de l'observation" required class="dp-InputText" name="InputDescriptionView">

            <label for="InputDateView">Date</label>
            <input type="date" id="InputDateView" data-date-format="jj.mm.aaaa" aria-describedby="DateHelp" placeholder="Date de l'observation" required class="dp-InputText" name="InputDateView">

            <label for="InputPlacementXView" >coordonnée X</label>
            <input type="text" id="InputPlacementXView" placeholder="Cordonnée X" required class="dp-InputText" name="InputPlacementXView">

            <label for="InputPlacementYView" >coordonnée Y</label>
            <input type="text" id="InputPlacementYView" placeholder="Cordonnée Y" required class="dp-InputText" name="InputPlacementYView">



<!--mise en place du bouton de confirmation de formulaire-->
            <input type="submit" value="Submit" class="btn btn-primary">

            <br>
            <p>Les images de 'observation pourront être ajoutées à l'observation dans le menu de modification de celle-ci</p>
        </form>
    </div>




<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>

