<?php
/**
 * Created by PhpStorm.
 * User: Michael.HOFER
 * Date: 11.03.2019
 * Time: 08:41
 */
ob_start();
$titre="DistilledPictures - Formulaire d'inscrption";
?>
<br>
<h1 class="dp-TitleOfPage">Rechercher une observation</h1>

<div class="dp-ThatFormulaire mx-auto">
    <p>Taper le Titre d'une observation afin de la retrouver</p>
    <form action="index.php?action=SearchViews" method="post" name="formSearch">
         <input type="text" id="InputSearch" aria-describedby="SearchHelp" placeholder="Titre de l'observation que je cherche" required class="dp-InputText" name="InputSearch">
        <br>
         <input type="submit" value="Submit" class="btn btn-primary">

    </form>
</div>
<div class="d-flex flex-wrap justify-content-around">
    <?php
    if(isset($contenu)){
        echo $contenu;
    }
    ?>
</div>
<?php

$contenu = ob_get_clean();
require "gabarit.php";
?>
