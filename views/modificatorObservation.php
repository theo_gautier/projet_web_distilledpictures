<?php
/**
 * Created by PhpStorm.
 * User: Michael.HOFER
 * Date: 11.03.2019
 * Time: 08:41
 */
ob_start();

$titre = "DistilledPictures - Formulaire d'inscrption";

?>
<h1>Modificator</h1>
<p>Modifier les champs que vous souhaitez puis cliquez sur envoyer pour mettre à jour la base de données</p>
<form action="controllers/updateobservationController.php" method="post">
    <input hidden value="<?php echo $_GET['id'] ?>" name="id">
    <label for="InputTitre">Titre</label>
    <input type="text" id="InputTitre" aria-describedby="TitreHelp" placeholder="Nouveaux Titre"
           value="<?php echo $observation['Nom'] ?>" class="dp-InputText" id="InputTitre" name="title">

    <label for="InputDescription">Description</label>
    <input type="text" id="InputDescription" aria-describedby="DescriptionHelp" placeholder="Nouvelle Descriptions"
           value="<?php echo $observation['Desc'] ?>" class="dp-InputText" id="InputDescription" name="desc">

    <label for="InputDate">Date</label>
    <input type="text" id="InputDate" aria-describedby="DateHelp" placeholder="Nouvelle Date"
           value="<?php echo $observation['Date'] ?>" class="dp-InputText" id="InputDate" name="date">

    <label for="InputCoordX">Coordonnée X</label>
    <input type="text" id="InputCoordX" value="<?php echo $observation['PosX'] ?>" aria-describedby="CoordXHelp"
           placeholder="Nouvelle Coordonnée X" class="dp-InputText" id="InputCoordX" name="x">

    <label for="InputCoordY">Coordonnée Y</label>
    <input type="text" id="InputCoordY" aria-describedby="CoordYHelp" value="<?php echo $observation['PosY'] ?>"
           placeholder="Nouvelle Coordonnée Y" class="dp-InputText" id="InputCoordY" name="y">


    <input type="submit" value="Envoyer " class="btn btn-primary" style="margin-top: 10px; margin-left: 10px">
</form>
<br>
<script type="text/javascript">

    function showForm() {
        document.getElementById('uploadPicture').hidden = false;
        $("html, body").animate({scrollTop: 1500}, 1500);
    }
</script>
<button onclick="showForm()" style="margin-bottom: 5px">Ajouter une image</button>

<form id="uploadPicture" action="controllers/uploadppictureController.php" method="post" enctype="multipart/form-data"
      hidden style="margin-left: 10px; margin-bottom: 10px">
    <input hidden value="<?php echo $_GET['id'] ?>" name="id">
    <label>Insérer image</label>
    <input type="file" name="picture">
    <input type="submit" value="envoyer">
</form>
<br>

<?php foreach ($observation['Photos'] as $picture) { ?>

    <form action="controllers/deleteimageController.php" method="post">
        <input hidden value="<?php echo $_GET['id'] ?>" name="id">
        <div class="container">


            <img src="views/resources/images/observations/<?php echo $_SESSION['userPseudo'] ?>/<?php echo $picture ?>"
                 width="200">
            <input hidden value="<?php echo $picture ?>" name="picture">
            <div class="middle">
                <input type="submit" class="text" value="Supprimer">
            </div>
        </div>
    </form>
<?php } ?>

</div>


<?php
if (isset($contenuImages)) {
    echo $contenuImages;
}
$contenu = ob_get_clean();
require "gabarit.php";
?>
