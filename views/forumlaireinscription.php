<?php
/**
 * Created by PhpStorm.
 * User: Michael.HOFER
 * Date: 11.03.2019
 * Time: 08:41
 */
ob_start();
$titre="DistilledPictures - Formulaire d'inscrption";
?>
<br>
<h1 class="dp-TitleOfPage">Formulaire d'inscription</h1>

    <?php
    if(isset($_POST["registererror"])){
        echo "<div class=\"alert alert-danger\" role=\"alert\"><h3 class=\"text-center font-weight-bolder align-middle\">une érreur à été rencontrée. Veuillez réesseyer.</h3></div>";
    }
    ?>

    <div class="dp-ThatFormulaire mx-auto">

        <form action="index.php?action=register" method="post" name="formRegister">

            <label for="InputPseudo">Pseudo *</label>
            <input type="text"  id="InputPseudo" aria-describedby="PseudoHelp" placeholder="Votre Pseudo" required class="dp-InputText" id="InputPseudo" name="InputPseudo">

            <label for="inputEmail">Adresse Email*</label>
            <input type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="Ex. Patrick.Martin@gmail.com" required class="dp-InputText" id="InputEmail" name="InputEmail">

            <label for="InputPrenom">Prénom *</label>
            <input type="text" id="InputPrenom" aria-describedby="PrenomHelp" placeholder="Votre Prénom" required class="dp-InputText" id="InputPrenom" name="InputPrenom">

            <label for="InputNom">Nom *</label>
            <input type="text" id="InputNom" aria-describedby="NomHelp" placeholder="Votre Nom" required class="dp-InputText" id="InputNom" name="InputNom">

            <label for="inputPassword" >Mot De Passe *</label>
            <input type="password" id="inputPassword" placeholder="Mot De Passe" required class="dp-InputText" id="InputPassword" name="InputPassword">

            <label for="inputVPassword" >confirmation du mot de passe *</label>
            <input type="password" id="inputVPassword" placeholder="Verification mot de passe" required class="dp-InputText" id="InputVPassword" name="InputVPassword">


            </select>

            <input type="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>




<?php
$contenu = ob_get_clean();
require "gabarit.php";
?>

