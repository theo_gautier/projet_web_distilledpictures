<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 20.03.2019
 * Time: 11:52
 * File: index.php
 */


session_start();
require "controllers/controller.php";
/*
 * permet d'appeler les fonctions du controlleur
 * */
if (isset($_GET['action'])) {
    $actionget = $_GET['action'];

    switch ($actionget) {
        case 'accueil':
            $_GET['action'] = $actionget;
            showAccueil();
            break;
        case 'login':
            $_GET['action'] = $actionget;
            Login();
            break;
        case 'register':
            $_GET['action'] = $actionget;
            Register();
            break;
        case 'logout':
            $_GET['action'] = $actionget;
            logout();
            break;
        case "LastViews":
            $_GET['action'] = $actionget;
            displayLastViews();
            break;

        case "SearchViews":
            $_GET['action'] = $actionget;
            displaySearchViews($_POST);
            break;

        case "MyViews":
            $_GET['action'] = $actionget;
            displayMyViews();
            break;
        case "SingleView":
            $_GET['action'] = $actionget;
            displaySingleView($_GET);
            break;
        case "modifier":
            $_GET['action'] = $actionget;
            displayModifyView($_GET['id']);
            break;
        case "supprimerObs":
            $_GET['action'] = $actionget;
            deleteObservation($_GET);
            break;
        case "createView":
            $_GET['action'] = $actionget;
            createView();
            break;
        case "sendEmail":
            $_GET['action'] = $actionget;
            sendMail();
            break;
        default:
            $_GET['action'] = 'accueil';
            showAccueil();
            break;
    }
} else {
    $_GET['action'] = 'accueil';
    showAccueil();
}